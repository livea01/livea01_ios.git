# tianma_live

[![CI Status](https://img.shields.io/travis/274947394@qq.com/tianma_live.svg?style=flat)](https://travis-ci.org/274947394@qq.com/tianma_live)
[![Version](https://img.shields.io/cocoapods/v/tianma_live.svg?style=flat)](https://cocoapods.org/pods/tianma_live)
[![License](https://img.shields.io/cocoapods/l/tianma_live.svg?style=flat)](https://cocoapods.org/pods/tianma_live)
[![Platform](https://img.shields.io/cocoapods/p/tianma_live.svg?style=flat)](https://cocoapods.org/pods/tianma_live)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

tianma_live is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'tianma_live'
```

## Author

274947394@qq.com, 274947394@qq.com

## License

tianma_live is available under the MIT license. See the LICENSE file for more info.
